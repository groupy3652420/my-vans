import axios from "../api/axios";
import useAuth from "./useAuth";
const useRefreshToken = () => {
  const { setAuth } = useAuth();
  const refresh = async () => {
    const response = await axios.get("/refresh", {
      withCredentials: true,
    });
    // this allow us to send cookies with our request wich
    // has the response token and it is a secure cookie that will never see in our
    // js but axios can send it to the backend endpoint
    setAuth((prev) => {
      console.log(JSON.stringify(prev));
      console.log(response.data.accessToken);
      return {
        ...prev,
        accessToken: response.data.accessToken,
      };
    });
    return response.data.accessToken;
    // this func should return new accessT to use it at req
    // we use this func when accessT expire
  };
  return refresh;
};
export default useRefreshToken;
