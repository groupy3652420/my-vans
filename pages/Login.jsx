import { useEffect, useRef, useState } from "react";
import { useNavigate, useLocation, Link } from "react-router-dom";

import axios from "../axios";
import useAuth from "../hooks/useAuth";

const LOGIN_URL = "/users/login";
export default function Login() {
  const { setAuth } = useAuth;
  const navigate = useNavigate;
  const location = useLocation;
  const from = location.state?.from?.pathname || "/";
  const [errMsg, setErrMsg] = useState("");
  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");
  const emailRef = useRef();
  useEffect(() => {
    emailRef.current.focus();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(
        LOGIN_URL,
        JSON.stringify({ email, password: pwd }),
        {
          headers: { "Content-Type": "application/json" },
          withCredentials: true,
        }
      );

      const accessToken = response?.data.accessToken;
      setAuth({ email, accessToken });
      setEmail("");
      setPwd("");
      navigate(from, { replace: true });
    } catch (err) {
      if (!err?.response) {
        setErrMsg("No Server response");
      } else if (err.response?.status === 400) {
        setErrMsg("Missing email or password");
      } else if (err.response?.status === 401) {
        setErrMsg("Unauthorized");
      } else {
        setErrMsg("Login failed");
      }
    }
  };

  return (
    <div className="login-container">
      <h1>Sign in to your account</h1>
      <p>{errMsg}</p>
      <form onSubmit={handleSubmit} className="login-form">
        <input
          name="email"
          autoComplete="off"
          required
          ref={emailRef}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          placeholder="Email address"
          value={email}
        />
        <input
          name="password"
          onChange={(e) => setPwd(e.target.value)}
          type="password"
          placeholder="Password"
          required
          value={pwd}
        />
        <button>Log in</button>
      </form>
    </div>
  );
}
