import React from "react";
import App from "./App";
import ReactDOM from "react-dom/client";
import AuthContext, { AuthProvider } from "./context/AuthProvider";

ReactDOM.createRoot(document.getElementById("root")).render(
  <AuthProvider>
    <App />
  </AuthProvider>
);
