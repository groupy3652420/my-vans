import { Navigate, Outlet, useLocation } from "react-router-dom";
import useAuth from "../hooks/useAuth";

export default function RequireAuth() {
  const { auth } = useAuth();
  const location = useLocation();
  const isLoggedIn = false;

  //  return(
  //   auth?.email
  //       ?<Outlet/>
  //       : <Navigate to="/login" state={{ from: location }} replace />; )
  if (!isLoggedIn) {
    return <Navigate to="/login" state={{ from: location }} replace />;
    //this will replace the login with were i was when i click the back button
  }
  return <Outlet />;
}
